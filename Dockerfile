# This file is a template, and might need editing before it works on your project.
FROM openjdk:8-jdk-alpine
ARG JAVA_FILE
COPY ${JAVA_FILE} app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","app.jar","--imageServer=http://54.224.161.40:8080/images/"]
EXPOSE 8080

